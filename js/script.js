// current menuitem
$(document).ready(function() {
    $(".menuItem[href]").each(function() {
        if (this.href == window.location.href) {
            $(this).addClass("current");
        }
    });
    $(".menuItem").click(function(){
        location.reload();
    });
});
// slider
$(document).ready(function() {
    if($(".prv").parent(".textOverImage").parent(".stockImage").parent(".box").attr('id') == "box1")
        $(".prv").prop('disabled', true);
    else
        $(".prv").prop('disabled', false);

    if($(".nxt").parent(".textOverImage").parent(".stockImage").parent(".box").attr('id') == "box3")
         $(".nxt").prop('disabled', true);
    else
        $(".nxt").prop('disabled', false);

    $(".nxt").click(function() {
        if($(window).width() >= 755){
            $(this).parent(".textOverImage").parent(".stockImage").parent(".box").animate({left: '-150%'}, 500 );
            $(this).parent(".textOverImage").parent(".stockImage").parent(".box").next(".box").animate({left: '20%'},500);
        } else{
            $(this).parent(".textOverImage").parent(".stockImage").parent(".box").animate({left: '-150%'}, 500 );
            $(this).parent(".textOverImage").parent(".stockImage").parent(".box").next(".box").animate({left: '5%'},500);
        }
    });
    $(".prv").click(function() {
        if($(window).width() >= 755){
            $(this).parent(".textOverImage").parent(".stockImage").parent(".box").animate({left: '150%'}, 500 );
            $(this).parent(".textOverImage").parent(".stockImage").parent(".box").prev(".box").animate({left: '20%'},500);
        } else{
            $(this).parent(".textOverImage").parent(".stockImage").parent(".box").animate({left: '150%'}, 500 );
            $(this).parent(".textOverImage").parent(".stockImage").parent(".box").prev(".box").animate({left: '5%'},500);
        }
    });
});

// Het switchen naar de doneerpagina
$('nav .donateButton').click(function(){
    window.open('doneer.html', '_self');
});

$(window).resize(function(){
    $('.row .box').each(function(){
        if($(this).attr('style') !== 'left: -150%;' && $(window).width() <= 755 && $(this).attr('style') === 'left: 20%;'){
            $("#" + this.id).css("left", "5%");
        } else if($(this).attr('style') !== 'left: -150%;' && $(window).width() >= 755 && $(this).attr('style') === 'left: 5%;'){
            $("#" + this.id).css("left", "20%");
        }
    })
})

// Het aanmaken van de counter variabele
if(sessionStorage.getItem('counter') !== null){
    var counter = sessionStorage.getItem('counter');
} else {
    var counter = 0;
}

// Het doneren
$('.doneer table tr td button').click(function(){
    var name = $('table tr td input[name="firstName"]').val();
    var geld = $('table tr td input[name="money"]').val();
    if(name.length !== 0){
        $('#nameError').text('')
        if(geld !== 0){
            $('#geldError').text('');
            var i = 0;
            if(sessionStorage.getItem('money') !== null){
                var total = Number(sessionStorage.getItem('money')) + Number(geld);
                sessionStorage.setItem("money", parseFloat(total).toFixed(2));
            }else {
                sessionStorage.setItem("money", geld);
            }
            $('#totaalD').text(sessionStorage.getItem('money'));
            var i = $('#doneer tr').length;
            if(i <= 5){
                printTr(name, geld);
            }else{
                $('#doneer tr:last').remove();
                printTr(name, geld);
            }
        } else {
            $('#geldError').text('Vul een aantal in')
        }
    } else {
        $('#nameError').text('Vul een naam in')
    }
});

// De donaties na het herladen van de pagina ophalen
$(document).ready(function(){
    if(sessionStorage.getItem('money') !== null){
        $('#totaalD').text(sessionStorage.getItem('money'));
        var counter = Number(sessionStorage.getItem('counter'));
        while(counter >= 0){
            if($('#doneer tr').length >= 6){
                $('#doneer tr:last').remove();
            }
            var name = sessionStorage.getItem('name' + counter);
            var geld = sessionStorage.getItem('geld' + counter);
            $('#doneer tr:nth-child(2)').before('<tr><td>' + name + '</td><td>' + geld + '</tr>');
            counter--;
        }
    }
});

// De regels printen
function printTr(name, geld){
    $('#doneer tr:first').after('<tr><td>' + name + '</td><td>' + geld + '</tr>');
    sessionStorage.setItem('name' + counter, name);
    sessionStorage.setItem('geld' + counter, geld);
    sessionStorage.setItem('counter', counter);
    counter++;
}

// Het contact form
$('.contact button').click(function(){
    var counter = 0;
    var aan = $('.contact [name="aanhef"]').val();
    if(aan.length !== 0){
        $('.contact #aanhefError').text('');
        counter++;
    } else {
        $('.contact #aanhefError').text('Je moet een aanhef invullen');
    }
    var name = $('.contact [name="name"]').val();
    if(name.length !== 0){
        $('.contact #nameError').text('');
        counter++;
    } else {
        $('.contact #nameError').text('Je moet een naam invullen');
    }
    var mail = $('.contact [name="mail"]').val();
    if(name.length !== 0){
        if(validateEmail(mail)){
            $('.contact #mailError').text('');
            counter++;
        } else {
            $('.contact #mailError').text('Je moet een juist email-adress invullen');
        }
    } else {
        $('.contact #mailError').text('Je moet een mail invullen');
    }
    var tel = $('.contact [name="tel"]').val();
    if(tel.length == 10){
        $('.contact #telError').text('')
        counter++;
    } else{
        $('.contact #telError').text('Het telefoon nummer is te kort of te lang')
    }
    var bericht = $('.contact [name="bericht"]').val();
    if(bericht.length !== 0){
        $('.contact #berichtError').text('');
        counter++;
    } else {
        $('.contact #berichtError').text('Je moet een bericht invullen');
    }
    if(counter === 5){
        alert('Het contactformulier is verstuurd');
        location.reload();
    }
});

$('input[type="checkbox"]').on('change', function() {
    $(this).siblings('input[type="checkbox"]').prop('checked', false);
    });

// De validatie voor de email
function validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
}


// responseve menu
$('.hamburger').click(function(){
    if ($('.mainMenu').is(":visible")) {
        $('.mainMenu').css('display', 'none');
    }
    else {
        $('.mainMenu').css('display', 'block');
    }

});
